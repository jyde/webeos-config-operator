package v1alpha1

import (
	"github.com/operator-framework/operator-lib/status"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +kubebuilder:validation:Enum=NoIndexing;IndexByCernCrawler;IndexByAllCrawlers
// +kubebuilder:validation:XPreserveUnknownFields

// CrawlingModeConfiguration configuration of the robots.txt
type CrawlingModeConfiguration string

// UserProvidedDirectorySpec defines the desired state of UserProvidedDirectory
type UserProvidedDirectorySpec struct {
	// +kubebuilder:validation:Pattern:="^/eos(/[^/]+){2,}$"
	// +kubebuilder:validation:Required

	// The existing EOS folder to publish. Follow KB0004096 to configure the EOS folder appropriately.
	EosPath string `json:"eosPath"`

	// This is a valid DNS name. OPA will limit further names that can be used.
	// NB: a DNS label consisting of only digits is not allowed, but we don't have backtracking in these regex so it's difficult to achieve!
	// +kubebuilder:validation:Pattern:="^([a-zA-Z]|[a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9])(\\.([a-zA-Z]|[a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9]))*$"
	// +kubebuilder:validation:Required

	// The desired site DNS name. Must look like mysite.web.cern.ch and not be used already by another site.
	SiteUrl string `json:"siteUrl"`

	// Global configuration of the webeos site
	GlobalSettings *GlobalSettings `json:"globalSettings,omitempty"`

	// Configuration of subfolders of the webeos site
	FolderConfiguration []WebeosSiteConfigurationPath `json:"folderConfiguration,omitempty"`

	// This setting only takes effect when no robots.txt is present in the web site's root folder.
	// +kubebuilder:default:="IndexByCernCrawler"
	AllowedWebCrawlersIfNoRobotsTxt CrawlingModeConfiguration `json:"allowedWebCrawlersIfNoRobotsTxt,omitempty"`
}

// GlobalSettings Global configuration of the webeos site
type GlobalSettings struct {

	// Sites not maintained anymore can be archived. A banner indicates the content might not be up to date, and only authenticated users can access it.
	// +kubebuilder:validation:Enum=true;false
	ArchiveSite bool `json:"archiveSite,omitempty"`

	// Sets whether anonymous access is enabled. Per-folder configuration always takes precedence over this setting.
	// When disabled, site visitors must authenticate to access the sites. By default, when anonymous access
	// is disabled, site access is restricted to users member of the EduGain Federation. Site access can be further restricted by defining required roles in
	// the Application Portal.
	// +kubebuilder:validation:Enum=true;false
	AllowAnonymousAccessByDefault bool `json:"allowAnonymousAccessByDefault,omitempty"`

	// Allows to override web server configuration using .htaccess files. Site access will be slower if enabled. If disabled, .htaccess files are ignored.
	// `true` results in `AllowOverride All`; `false` results in `AllowOverride None`.
	// +kubebuilder:validation:Enum=true;false
	UseDistributedConfigurationFiles bool `json:"useDistributedConfigurationFiles,omitempty"`
}

// UserProvidedDirectoryStatus defines the observed state of UserProvidedDirectory
type UserProvidedDirectoryStatus struct {
	// Conditions represent the latest available observations of an object's state
	Conditions status.Conditions `json:"conditions"`
}

const (
	// UpdReady indicates that the the ApplicationRegistration was successful and the WebeosSite CR was created
	UpdReady status.ConditionType = "Ready"
	// UpdFailure indicates that there was a problem with either the ApplicationRegistration or the creation of the WebeosSite CR
	UpdFailure status.ConditionType = "Failure"
)

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status

// UserProvidedDirectory is the Schema for the userprovideddirectories API
type UserProvidedDirectory struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   UserProvidedDirectorySpec   `json:"spec,omitempty"`
	Status UserProvidedDirectoryStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// UserProvidedDirectoryList contains a list of UserProvidedDirectory
type UserProvidedDirectoryList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []UserProvidedDirectory `json:"items"`
}

func init() {
	SchemeBuilder.Register(&UserProvidedDirectory{}, &UserProvidedDirectoryList{})
}
