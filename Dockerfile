# Build the manager binary
FROM golang:1.15.4 as builder

WORKDIR /workspace

# Copy the Go Modules manifests
COPY go.mod go.mod
COPY go.sum go.sum
COPY vendor/ vendor/

# Copy the go source
COPY main.go main.go
COPY controllers/ controllers/

# Build
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GO111MODULE=on go build -a -mod=vendor -o manager main.go

# Use ubi-minimal instead of the default distroless image
# to make sure that we have `test` binary which is used for readiness probes
FROM registry.access.redhat.com/ubi8/ubi-minimal:latest
WORKDIR /
COPY --from=builder /workspace/manager .

# Copy vhost template
COPY conf /conf

USER nonroot:nonroot
ENTRYPOINT ["/manager"]
