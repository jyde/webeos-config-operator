# WebEOS config operator

Controller using OperatorSDK (v1.1.0) for the generation of Apache Vhost config files.
This operator will watch the custom resources of WebeosSite in the Webeos namespace on OpenShift and accordingly generate the vhost config files.

## Required Variables
The required variables for testing are:

 var |  description
 --- |  ---
`OIDC_CLIENT_APP_MANAGEMENT_URL` | Link to the management page of the OIDC application
`OIDC_CLIENT_ID` | Needed to test OIDC behaviour
`OIDC_CLIENT_SECRET` | Needed to test OIDC behaviour
`USER` | Dummy User to validate the correct protection of the website
`USER_PASSWORD`  | Dummy User's password

For the `OIDC_CLIENT_APP_MANAGEMENT_URL`, the value should be the link to the
Application in the Application Portal
(Ex:https://application-portal.web.cern.ch/manage/01d7p121c-i84e-c83o-1107-ed1b1cb91239b).

To create the OIDC credentials, you will need to create an Application first following
these steps: https://auth.docs.cern.ch/applications/application-configuration/.
After, create an API Access: https://auth.docs.cern.ch/user-documentation/oidc/api-access/


To create a service account: https://account.cern.ch/account/Management/NewAccount.aspx

For the Application, the Administrator Group should be set as `web-services`.
For both Application and Service Account, the description should have "Used for
CI of https://gitlab.cern.ch/webservices/webeos/webeos-config-operator"

## Local development
To run the controller locally, you can run through terminal:
```bash
operator-sdk run local $KUBECONFIG --watch-namespace "" --operator-flags "--label-selector mylabel=myvalue --vhost-template-path /PathToRepo/webeos-config-operator/conf/vhost_config.template --apache-path /ApachePath --oidc-cryptopassphrase test --readiness-probe-path /PathToRepo/ImReady"

```

Alternatively, a JSON example to run the controller on VS Code or similar:
```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Launch",
            "type": "go",
            "request": "launch",
            "mode": "auto",
            "program": "${fileDirname}",
            "env": {
                "KUBECONFIG":"KubeconfigPath",
                "WATCH_NAMESPACE":"",
                "LABEL":"mylabel=myvalue",
            },
            "args": [
                "--label-selector", "mylabel=myvalue",
                "--apache-path", "/ApachePath",
                "--vhost-template-path", "/PathToRepo/webeos-config-operator/conf/vhost_config.template",
                "--oidc-cryptopassphrase", "PassPhrase",
                "--readiness-probe-path", "/PathToRepo/webeos-config-operator/customResourcesReady",
            ]
        }
    ]
}
```
Note:
- --label-selector, label filter to select only the webeos sites assigned to the deployment the operator is running in
- --apache-path, flag containing the path of the Apache Server folder (This will be the path used by the Operator to store the generated Vhost files)


## How to do manual tests on a webeos site

1. Get User account credentials by accessing the [Gitlab Variables](https://gitlab.cern.ch/webservices/webeos/webeos-config-operator/-/settings/ci_cd)

2. Create a directory on EOS with content to serve by following these steps:
```bash
# SSH into $USER EOS directory
$ export USER=<user_from_gitlab_variable>
$ export LETTER=<first_letter_of_user>
$ ssh -t $USER@lxplus.cern.ch "cd /eos/user/$LETTER/$USER/www; bash"

# Create the directory which will serve the content
$ mkdir <directory_name>
$ cd <directory_name>
# Keep the EOS path of your site
$ pwd

# Create an .htaccess file
# Docs for .htaccess: https://webeos.docs.cern.ch/authorization/#provide-access-to-an-application-role
$ vi .htaccess

# Create landing page
$ echo "Hello, this is your landing page" > index.html
```

3. Edit `tests/userProvidedDirectory.yaml`:
```yaml
    useDistributedConfigurationFiles: true
```

4. Export the `KUBECONFIG` of your okd4 dev cluster
```bash
$ EXPORT KUBECONFIG=<full_path_of_kubeconfig>
```

5. Run `tests/createTestSite.sh` script
```bash
$ ./tests/createTestSite.sh -u <your_username> -p <eos_path>
```

6. Get url of your site with:
```bash
$ oc get routes -A | grep <SITE_NAME>
```

## Change vhost configuration directly on your okd4 dev cluster

1. Choose the website for which you want to change the `vhost`:
 ```bash
 $ oc get webeossites.webeos.webservices.cern.ch -n webeos
 ```

2. Find on which deployment it's being served:
 ```bash
 $ oc get webeossites.webeos.webservices.cern.ch/<website_name> -n webeos -o json | jq -r ' .spec.serverDetails.assignedDeploymentShard'
 ```

3. Get pod serving the content of the deployment:
 ```bash
 $ oc get pods -l=app=<deployment_name> -n=webeos
 ```

4. Access the `httpd` container running inside the Pod:
```
kubectl exec --stdin --tty <pod_name> -c httpd -n=webeos -- /bin/bash
```

5. Edit the Vhost of the website:
```
vi /etc/httpd/conf.d/vhost/$WEBSITE.conf
```

6. Gracefully restart the `httpd`:
```
kill -USR1 1
```
