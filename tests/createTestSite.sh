#!/bin/bash
usage() { echo "Usage: $0 [--username <CERN_USER>] [--path <EOSPATH>]" 1>&2; exit 1; }

# Options

ARGS=$(getopt -o 'u:p:' --long 'username:,path:' -- "$@") || exit 1

eval "set -- $ARGS"

while true; do
  case "$1" in
    (-u|--username)
      CERN_USER="$2"; shift 2;;
    (-p|--path)
      EOSPATH="$2"; shift 2;;
    (--) shift; break;;
    (*) usage;;
  esac
done

if [[ -z $CERN_USER ]]; then
        echo "missing user"
        usage
fi
if [[ -z $EOSPATH ]]; then
        echo "missing path"
        usage
fi
export EOSPATH

oc get user $CERN_USER || oc create user $CERN_USER
echo "Using CERN_USER: ${CERN_USER}"

export NAME=webeos-test-${RANDOM}
echo "Using SITE_NAME: ${NAME}"

function create_site {
    NAME=$1
    export PROJECT=$NAME
    export CR_NAME=$NAME
    export DOMAIN=webtest.cern.ch
    export FQDN=${NAME}.${DOMAIN}
    oc new-project ${PROJECT} --description 'Manual webeos test $NAME' --as $CERN_USER --as-group system:authenticated:oauth --as-group system:authenticated >/dev/null
    oc label namespace ${PROJECT} manual-run=${NAME}
    envsubst < tests/userProvidedDirectory.yaml | oc create --as $CERN_USER -n ${PROJECT} -f -
}

create_site ${NAME}

echo "Created site ${NAME}"
echo "To delete it, use: oc delete project -l manual-run=${NAME}"
