module gitlab.cern.ch/webservices/webeos/webeos-config-operator

go 1.15

require (
	github.com/Masterminds/goutils v1.1.1 // indirect
	github.com/Masterminds/semver v1.5.0 // indirect
	github.com/Masterminds/sprig v2.22.0+incompatible
	github.com/go-logr/logr v0.1.0
	github.com/huandu/xstrings v1.3.2 // indirect
	github.com/mitchellh/copystructure v1.1.1 // indirect
	github.com/openshift/api v3.9.0+incompatible
	github.com/spf13/pflag v1.0.5
	gitlab.cern.ch/webservices/webeos/webeos-site-operator v0.0.0-20210302094903-c9f63b4933e2
	k8s.io/apimachinery v0.18.6
	k8s.io/client-go v0.18.6
	sigs.k8s.io/controller-runtime v0.6.4
)
